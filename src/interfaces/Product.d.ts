import { IGet, IGetArgs } from '@/interfaces/Rest'

export interface IProduct {
  id: number
  name: string
  image: string
}

export interface IGetProducts extends IGet {
  data: IProduct[]
}

export interface IGetProduct extends IGet {
  data: IProduct
}

export interface IGetProductArgs extends IGetArgs {
  id?: number | number[]
}
