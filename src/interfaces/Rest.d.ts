export interface IGet {
  total: number
}

export interface IGetArgs {
  limit?: number | 'all'
  offset?: number
  order?: TOrder
}

type TOrder = 'ASC' | 'DESC'
