import { IGet, IGetArgs } from '@/interfaces/Rest'

export interface IPizza {
  id: number
  name: string
  description: string
  productID: number
  createdAt: Date
  categoryIDs: number[]
  price: number
  types: any[]
  images: any[]
}

export interface IGetPizzas extends IGet {
  data: IPizza[]
}

export interface IGetPizza extends IGet {
  data: IPizza
}

export interface IGetPizzaArgs extends IGetArgs {
  id?: number | number[]
  categoryID?: number | number[]
}
