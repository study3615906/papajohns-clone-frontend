import React from 'react'
import styles from './Loader.module.scss'

export const Loader = () => {
  return (
    <div className={styles.loaderWrapper}>
      {/*<span className="loader"></span>*/}
      <div className={styles.center}>
        <div className={styles.ring}></div>
        <span>loading...</span>
      </div>
    </div>
  )
}
